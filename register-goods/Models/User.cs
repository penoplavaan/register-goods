﻿using System;
namespace register_goods.Models
{
    

    public class User: Model
	{
        public enum UserTypes : uint
        {
            user = 10,
            transportOrganization = 20,
            veterinaryClinicMinitipal = 31,
            veterinaryClinicPrivate = 32,
            veterinaryClinicGoverment = 33,
            sellingAnimalsGoodsOrg = 40
        }

        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public UserTypes UserType { get; set; }
        public int OrgainsationId { get; set; }
    }
}


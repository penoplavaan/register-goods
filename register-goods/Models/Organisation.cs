﻿using System;
namespace register_goods.Models
{
    

    public class Organisation : Model
    {
        public enum OrganisationTypes : uint
        {
            shelter = 10,
            transportOrganization = 20,
            veterinaryClinicMinitipal = 31,
            veterinaryClinicPrivate = 32,
            veterinaryClinicGoverment = 33,
            sellingAnimalsGoodsOrg = 40
        }

        public string Name { get; set; }
        public string Address { get; set; }
        public int Inn { get; set; }
        public int Kpp { get; set; }
        public OrganisationTypes OrganisationType { get; set; }
        //public List<User> Users { get; set; }
        //public List<Product> Products { get; set; }
    }
}


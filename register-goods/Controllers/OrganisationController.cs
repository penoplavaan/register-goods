﻿using Microsoft.AspNetCore.Mvc;
using register_goods.Data;
using register_goods.Models;
using System.Text.Encodings.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace register_goods.Controllers
{
    public class OrganisationController
    {
        ApplicationDbContext context = new ApplicationDbContext();
        DbSet<Organisation> Organisations = new ApplicationDbContext().Organisation;

        public List<Organisation> Index()
        {
            return Organisations.ToList();
        }

        public List<Organisation> PaginatedIndex(int pageNumber, int pageSize = 10)
        {
            return GetPage(Organisations.ToList(), pageNumber, pageSize);
        }

        protected List<Organisation> GetPage(IList<Organisation> list, int pageNumber, int pageSize = 10)
        {
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public Organisation Add(string Name, string Address, int Inn, int Kpp, Organisation.OrganisationTypes OrganisationType)
        {
            var organisation = new Organisation { Name = Name, Address = Address, Inn = Inn, Kpp = Kpp, OrganisationType = OrganisationType };

            context.Add(organisation);
            int count = context.SaveChanges();

            if (count != 0) return organisation;
            return null;
        }

        public Organisation? Get(int id)
        {
            Organisation record = Organisations.Find(id);
            return record;
        }

        public Organisation? Update(int id, string? Name, string? Address, int? Inn, int? Kpp, Organisation.OrganisationTypes? OrganisationType)
        {
            var organisation = context.Organisation.First(a => a.Id == id);
            organisation.Name = Name;

            int count = context.SaveChanges();

            if (count != 0) return organisation;
            return null;
        }

        public void Delete(int id)
        {
            context.Remove(context.Organisation.Single(a => a.Id == id));
            context.SaveChanges();

            context.SaveChanges();

        }

        //public ActionResult Welcome(string name, int numTimes = 1)
        //{
        //    ViewBag.Message = "Hello " + name;
        //    ViewBag.NumTimes = numTimes;

        //    return View();
        //}

    }
}




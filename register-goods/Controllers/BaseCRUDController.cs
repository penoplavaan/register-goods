﻿using Microsoft.AspNetCore.Mvc;
using register_goods.Data;
using register_goods.Models;
using System.Text.Encodings.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Web;

namespace register_goods.Controllers
{
    public class BaseCRUDController<T> : Controller
        where T : Model,  new()
    {
        public  DbSet<T> Records { get; set; }
        public ApplicationDbContext context = new ApplicationDbContext();

        // 
        // GET: /User/ 

        public List<T> Index()
        {
            var rd = new BaseCRUDController<T>().Records;
            return rd.ToList();
        }

        public List<T> PaginatedIndex(int pageNumber, int pageSize = 10)
        {
            return !Equals(Records.ToList(), null) ? GetPage(Records.ToList(), pageNumber, pageSize) : new List<T> { };
        }

        protected List<T> GetPage(IList<T> list, int pageNumber, int pageSize = 10)
        {
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public T GetRecord(int id)
        {
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
            T record = Records.Where(b => b.Id == id)
                            .FirstOrDefault();
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
#pragma warning disable CS8603 // Possible null reference return.
            return record;
#pragma warning restore CS8603 // Possible null reference return.
        }
        public T AddRecord(T record)
        {
            Records.Add(record);
            context.SaveChanges();

            return record;
        }

        public T UpdateRecord(T record)
        {

            Records.Update(record);
            context.SaveChanges();

            return record;
        }

        public void DeleteRecordasync(int id)
        {
            Records.Remove(Records.Find(id));
            context.SaveChanges();
        }

    }
}

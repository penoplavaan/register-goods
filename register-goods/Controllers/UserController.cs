﻿using Microsoft.AspNetCore.Mvc;
using register_goods.Data;
using register_goods.Models;
using System.Text.Encodings.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace register_goods.Controllers
{
    public class UserController 
    {
        ApplicationDbContext context = new ApplicationDbContext();
        DbSet<User> Users = new ApplicationDbContext().User;

        public List<User> Index()
        {
            return Users.ToList();
        }

        public List<User> PaginatedIndex(int pageNumber, int pageSize = 10)
        {
            return GetPage(Users.ToList(), pageNumber, pageSize);
        }

        protected List<User> GetPage(IList<User> list, int pageNumber, int pageSize = 10)
        {
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public User Add(string Name, string Login, string Password, User.UserTypes UserType, int OrgainsationId)
        {
            var user = new User { Name = Name, Login = Login, Password = Password, UserType = UserType, OrgainsationId = OrgainsationId };
   
            context.Add(user);
            int count = context.SaveChanges();

            if (count!=0)  return user;
            return null;
        }

        public User? Get(int id)
        {
            User record = Users.Find(id);
            return record;
        }

        public User? Update(int id, string? Name, string? Login, string? Password, User.UserTypes? UserType, int? OrgainsationId)
        {
            var user = context.User.First(a => a.Id == id);
            user.Name = Name;

            int count = context.SaveChanges();

            if (count != 0) return user;
            return null;
        }

        public void Delete(int id)
        {
            context.Remove(context.User.Single(a => a.Id == id));
            context.SaveChanges();

            context.SaveChanges();

        }

        //public ActionResult Welcome(string name, int numTimes = 1)
        //{
        //    ViewBag.Message = "Hello " + name;
        //    ViewBag.NumTimes = numTimes;

        //    return View();
        //}

    }
}




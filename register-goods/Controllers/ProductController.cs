﻿using Microsoft.AspNetCore.Mvc;
using register_goods.Data;
using register_goods.Models;
using System.Text.Encodings.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace register_goods.Controllers
{
	public class ProductController
	{
        ApplicationDbContext context = new ApplicationDbContext();
        DbSet<Product> Products = new ApplicationDbContext().Product;

        public List<Product> Index()
        {
            return Products.ToList();
        }

        public List<Product> PaginatedIndex(int pageNumber, int pageSize = 10)
        {
            return GetPage(Products.ToList(), pageNumber, pageSize);
        }

        protected List<Product> GetPage(IList<Product> list, int pageNumber, int pageSize = 10)
        {
            return list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public Product Add(string Name, int Amount, float Price, int OrganisationId)
        {
            var product = new Product { Name = Name, Amount = Amount, Price = Price, OrganisationId = OrganisationId };

            context.Add(product);
            int count = context.SaveChanges();

            if (count != 0) return product;
            return null;
        }

        public Product? Get(int id)
        {
            Product record = Products.Find(id);
            return record;
        }

        public Product? Update(int id, string? Name, int? Amount, float? Price, int? OrganisationId)
        {
            var product = context.Product.First(a => a.Id == id);
            product.Name = Name;

            int count = context.SaveChanges();

            if (count != 0) return product;
            return null;
        }

        public void Delete(int id)
        {
            context.Remove(context.Product.Single(a => a.Id == id));
            context.SaveChanges();

            context.SaveChanges();

        }

        //public ActionResult Welcome(string name, int numTimes = 1)
        //{
        //    ViewBag.Message = "Hello " + name;
        //    ViewBag.NumTimes = numTimes;

        //    return View();
        //}
    }
}


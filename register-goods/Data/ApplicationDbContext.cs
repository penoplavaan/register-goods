﻿using System;
using Microsoft.EntityFrameworkCore;
using register_goods.Models;

namespace register_goods.Data
{
	public class ApplicationDbContext: DbContext
	{
		public virtual DbSet<User> User { get; set; }
		public virtual DbSet<Organisation> Organisation { get; set; }
		public virtual DbSet<Product> Product { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
			optionsBuilder.
				UseMySql("server=127.0.0.1;port=3306;user=root;database=un1;password=elephant3rEe",
				new MySqlServerVersion(new Version(8, 0, 11))
				)
				.UseLoggerFactory(LoggerFactory.Create(b => b
					.AddConsole().
					AddFilter(level => level >= LogLevel.Information)))
				.EnableSensitiveDataLogging()
				.EnableDetailedErrors();
        }
	}
}

